\chapter{\acf{IEV}: il microservizio implementato e la sua pipeline} 
\chaptermark{IEV}

\section{Cos'è \acf{IEV}?}

\textbf{IEV} è un microservizio realizzato per testare le variabili d'ambiente di uno o più container. Esso stesso è pensato per essere leggero e pronto all'uso attraverso Docker attraverso la creazione automatica di un'immagine da soli 8MB.\\

Realizzato in linguaggio Go, prevede una \ac{REST} \ac{API}, nello specifico una \textbf{GET}, sul percorso \textit{/env} ricevendo come parametri il nome del container, le variabili d'ambiente e i relativi valori da testare. Effettuato il test, il microservizio risponderà mostrando un esito positivo o negativo.\\

IEV è un semplice esempio che ci permette di capire quanto sia facile creare un microservizio e la relativa pipeline per buildarlo, testarlo e crearne l'immagine Docker. 

\clearpage 

\section{Strumenti utilizzati}

Per la realizzazione di \textbf{IEV} è stato utilizzato Go, un linguaggio di programmazione open source creato da Google che rende semplice la creazione di \wrapl{GoLogo}software affidabile ed efficiente. Successivamente è stato utilizzato \textbf{Jenkins}, installato in un container \textbf{Docker}, per la pipeline automatica. Inoltre, per la fase di testing, è stato utilizzato \textbf{Robot Framework}, installato in un container.

\subsection{Docker}

Avendo già parlato in parte del funzionamento di Docker (\ref{dockersubsec}), vediamo nel dettaglio i tre componenti principali di cui è fatto:

\begin{itemize}
    \parbox{0.5\textwidth}{
    \item \textbf{Docker Engine}: è la tecnologia sviluppata da Docker, che permette di creare container attraverso le immagini e di gestirli attraverso \acf{CLI}.}\parbox{0.1\textwidth}{~}\parbox{0.3\textwidth}{ \includegraphics[width=0.3\textwidth]{DockerLogo}}
    \item \textbf{Docker Image}: è un template avente un set di istruzioni grazie alle quali vengono creati i container. Ogni immagine è composta da una base a cui sono stati aggiunti componenti attraverso le istruzioni, anche dette \textit{livelli dell'immagine}. Per realizzare un'immagine viene scritto il cosiddetto \textit{Dockerfile}.
    \item \textbf{Docker Registry}: è usato per archiviare o scaricare le immagini. Il registry può essere anche pubblico, famosi esempi sono dati dal Docker Hub o dal registry di Red Hat. Inoltre, è possibile creare il proprio registry personale così da conservare immagini personalizzate in un luogo più sicuro.
\end{itemize}

Inoltre, Docker permette la creazione di \textbf{volumi} per avere file persistenti e condivisi tra più container e di \textbf{network} per far comunicare più containers tra di loro come se facessero parte di un'unica rete. 

\subsection{Jenkins}

\wrapl{JenkinsLogo}Scritto in Java, \textbf{Jenkins} nasce oltre 10 anni fa come server open source per l'automazione offrendo una vasta gamma di plugin che permettono di aggiungere varie funzionalità.\\ 

Una pipeline Jenkins può essere realizzata attraverso due metodi: interfaccia grafica, fornita dal plugin \textbf{BlueOcean} e il cosiddetto \textit{Jenkinsfile} scritto manualmente ed importato successivamente.  \\ 

Al fine della realizzazione di IEV, Jenkins è stato pensato per collegarsi a \textit{Git} in modo tale da scaricare il codice automaticamente dopo ogni push su \textit{master} ed iniziare il processo di build, test e containerizzazione sfruttando container temporanei e il Jenkinsfile da noi scritto. \\  


\subsection{Robot Framework}

\wrapr{RobotLogo}\textbf{Robot Framework} è un framework per l'automazione di test generici; ha una sintassi facile da usare ed utilizza un approccio a \textit{keywork}. Offre un set di librerie base che possono essere espanse creandole in Java o Python.\\ 

Un test robot può essere scritto attraverso interfaccia grafica o creando un \textit{file .robot} che sarà eseguito da \ac{CLI}.

\clearpage 

\section{La pipeline}

Prima di realizzare la pipeline di IEV, abbiamo avviato Jenkins su Docker usando il seguente comando: 

\lstset{basicstyle=\small}

\begin{lstlisting}[language=bash]
 docker run -u root -p 8080:8080 -p 50000:50000 -v jenkins-data:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v build-data:/var/jenkins_home/build --privileged --name jenkins_container  jenkinsci/blueocean
\end{lstlisting}

che andrà a scaricare l'immagine \texttt{jenkinsci/blueocean} dal Docker Hub e ad avviarla in un container chiamato \texttt{jenkins\_container} utilizzando i volumi \textbf{jenkins-data} e \textbf{build-data} che conterranno rispettivamente i dati di Jenkins e i dati di IEV. Inoltre, saranno condivise le porte 8080 e 50000 con l'host, l'utente root e il socket per la comunicazione con il Docker Engine. \\


\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{pipelinetotal}
    \caption{La nostra pipeline}
\end{figure}


Avviato e configurato inizialmente Jenkins, abbiamo legato il nostro repository Git in modo tale da scaricare il codice da compilare, il Jenkinsfile, il Dockerfile e il file robot. Una volta scaricato il tutto la pipeline si avvierà automaticamente.
\clearpage
\subsection{Go Build: creiamo il binario}

\begin{figure}[ht]
	\centering
    \includegraphics[width=0.7\textwidth]{gobuild1}
    \caption{Gli stages di "Go Build"}
\end{figure}

Iniziata la pipeline, incontreremo gli stages facenti parte del gruppo \textbf{Go Build} che utilizzano un container docker comune basato sull'immagine \texttt{golang:latest} a cui viene passato il volume build-data. Gli stages in questo punto sono i seguenti:

\begin{itemize}
    \item \textbf{Environment Preparation}: che prepara il container golang scaricando eventuali dipendenze come librerie e quant'altro.
    \begin{lstlisting}[language=bash]
    go get -v -u github.com/docker/docker/client
    go get -v -u golang.org/x/lint/golint
    \end{lstlisting}
    \item \textbf{Format}: che sfruttando i tool \texttt{go fmt} e \texttt{go vet} formatta ed analizza il codice scritto.
    \begin{lstlisting}[language=bash]
    go fmt main.go
    go vet main.go
    \end{lstlisting}
    \item \textbf{Test}: sfruttando il tool \texttt{golint} che verifica la correttezza del nostro codice e suggerisce miglioramenti nel caso vi siano.
    \begin{lstlisting}[language=bash]
    golint main.go
    \end{lstlisting}
    \item \textbf{Build}: builda il codice sfruttando flag particolari per rendere il binario libero dalla dipendenza di librerie esterne e lo sposta nel volume build-data per renderlo utilizzabile alle fasi successive.
    \begin{lstlisting}[language=bash]
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o IEVMicro -v -a -ldflags '-extldflags "-static"' .
    mv IEVMicro build/
    \end{lstlisting}
\end{itemize}

\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{gobuild2}
    \caption{Steps di Go Build}
\end{figure}

\subsection{Creazione della Docker Image}

\wrapl{cdiicon}Salvato il binario, Jenkins provvede alla distruzione del container provvisorio e procede con lo stage \textbf{Create Docker image} dove crea l'immagine basata sul nostro Dockerfile:\\

\begin{lstlisting}[language=docker]
FROM scratch
ADD IEVMicro  /
ENTRYPOINT ["/IEVMicro"]
\end{lstlisting}

che semplicemente, basandosi su un'immagine vuota, copia il binario precedentemente compilato. 

\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{dockerimagecreate}
    \caption{Creazione dell'immagine Docker}
\end{figure}


A questo punto, Jenkins avvia un container chiamato \texttt{iev\_container} basata sull'immagine \texttt{iev} che condivide la porta 6969 e il socket per comunicare con il Docker Engine. Inoltre crea una nuova network che permetterà di far comunicare il container IEV con quello di test.

\begin{lstlisting}[language=bash]
sh 'cp $JENKINS_HOME/build/IEVMicro .'
sh 'docker network create IEV'
docker.build("iev")
sh 'docker run -d --privileged -v /var/run/docker.sock:/var/run/docker.sock -p 6969:6969 --net IEV --name iev_container iev'
\end{lstlisting}

\subsection{Test con Robot Framework}

\wrapr{rfticon}Una volta avviato il container di IEV, bisogna testarne il corretto funzionamento sfruttando \textbf{Robot Framework}. A questo punto creeremo un container temporaneo utilizzando l'immagine \texttt{ppodgorsek/robot-framework} che accedendo alla network creata in precedenza andrà a comunicare con il container di IEV. \\ 

Robot Framework eseguirà una GET di test scritta nel suo file robot:
\lstset{basicstyle=\tiny}
\begin{lstlisting}[language=bash]
Get Requests
    Create Session  IEVTest    http://iev_container:6969
    &{params} =   Create Dictionary   name=jenkins_container   JENKINS_HOME=/var/jenkins_home  
    ${resp}=    Get Request     IEVTest   /env   params=${params}
    Log To Console  ${resp.status_code}
    Log To Console  ${resp.text}
    Should Be Equal As Strings    ${resp.text}    OK
\end{lstlisting}

dove creerà una sessione con \texttt{iev\_container}, un dominio risolto dal Docker Engine indicante il container di IEV, e a cui invierà una richiesta GET avente come parametri il nome \texttt{jenkins\_container} e il valore della variabile \texttt{JENKINS\_HOME}. \\

Ricevuta la richiesta, IEV risponderà \textit{OK} se le variabili d'ambiente e i loro valori coincidono, \textit{Environment Variable not found} se ci sono problemi sulle variabili d'ambiente e \textit{Container Not Found} se non è stato trovato il container indicato. \\

\subsection{Finalizzazione Pipeline}

\wrapl{rcicon}Finiti i test sul nostro microservizio, Jenkins provvederà ad eliminare i container temporanei creati e la network creata pulendo l'ambiente host da tutti i test effettuali, lasciando unicamente l'immagine \texttt{iev} \textbf{pronta all'uso.}

\lstset{basicstyle=\small}
\begin{lstlisting}[language=bash]
    sh 'docker stop iev_container'
    sh 'docker rm iev_container'
    sh 'docker network rm IEV'
\end{lstlisting}

\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{removecontainer}
    \caption{Rimozione dei container e network}
\end{figure}

