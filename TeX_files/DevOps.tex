\chapter{Microservizi e DevOps: una rivoluzione nella cultura aziendale}

\chaptermark{Microservizi e DevOps}


\section{I microservizi}

I \textbf{microservizi} rappresentano un nuovo modo di sviluppare applicazioni che va a staccarsi dall'approccio monolitico, semplicemente dividendo lo sviluppo dell'applicazione in blocchi indipendenti, rappresentanti una singola funzione. 

\quotingaf{"Microservices are an architectural and organizational approach to software development where software is composed of small independent services that communicate over well-defined APIs. These services are owned by small, self-contained teams." \\ \hspace*{\fill} Amazon Web Services \cite{amazonMicroservicesWeb}}

\subsection{Il passaggio da monolitico a microservizi}

Immaginiamo di sviluppare un prodotto, inizialmente lo sviluppo prevederà una singola applicazione contenente poche funzionalità. Con il crescere del progetto, lo sviluppo includerà nuovi casi d'uso e funzionalità che andranno inesorabilmente ad aumentare il numero di linee di codice. Dopo alcuni mesi o anni di sviluppo, ciò che era una semplice applicazione tende a diventare ciò che viene definito \textbf{sistema monolitico}: un insieme di funzionalità che rappresenta un problema nello sviluppo. \\ 

\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{monolithic-vs-microservices}
    \caption{Sistema monolitico contro microservizi \cite{redHatMicroservices}.}
\end{figure}

La grandezza e la complessità dell'applicazione rende difficile la comprensione nell'interezza ad un singolo sviluppatore, rallentando la velocità di risoluzione dei problemi e dello sviluppo di nuove funzionalità. Inoltre, essendo unito in un unico servizio, un sistema monolitico è difficile da scalare in caso di maggior uso di una specifica funzionalità. \\ 

\subsection{Caratteristiche dei microservizi}

Al contrario dei sistemi monolitici, un'\textbf{architettura a microservizi} prevede un'applicazione composta da diversi servizi, ognuno atto a svolgere una singola funzionalità. Ogni microservizio condivide delle caratteristiche comuni:

\begin{itemize}
    \item \textbf{Autonomia}: ogni singolo microservizio può essere sviluppato, messo in produzione e gestito in maniera diversa senza influenzare il comportamento di altri servizi. Il singolo microservizio diventa una \textbf{black box}, essendo indipendente dagli altri per quanto riguarda l'implementazione delle funzionalità e il linguaggio utilizzato per implementarle.
    \item \textbf{Specializzazione}: nascendo per risolvere un determinato problema, un microservizio può includere codice specializzato in modo tale da svolgere la propria funzionalità nel miglior modo possibile. 
\end{itemize} 

\subsection{I benefici dei microservizi}

I benefici offerti da un'architettura a microservizi contro il software monolitico sono molteplici.

\subsubsection{Agilità e qualità}

I microservizi aprono alla possibilità di creare piccoli team attivi nello sviluppo di singoli servizi e funzionalità. Ciò permette agli sviluppatori di comprendere pienamente la struttura del codice, migliorarne la qualità e  velocizzare il proprio lavoro. 

\subsubsection{Innovazione}

La possibilità di creare piccoli team unita al fatto di dover creare microservizi autonomi, permette di esplorare tecnologie, framework e strumenti innovativi. 

\subsubsection{Scalabilità e disponibilità}

La possibilità di creare grandi sistemi sfruttando piccoli servizi che assolvono ad una specifica funzionalità, permette di scalare su più server facilmente lavorando unicamente sulle funzionalità utilizzate maggiormente. Ciò permette anche di avere isolamento negli errori, dando la possibilità di lavorare su una singola funzionalità mentre tutte le altre sono attive e funzionati. 

\subsection{Comunicazione tra microservizi}

In un approccio monolitico, ogni componente invoca un altro attraverso metodi e chiamate a funzione. In contrasto, in un'architettura a microservizi abbiamo entità diverse che hanno bisogno di comunicare tra di loro usando meccanismi di comunicazione inter-processo o \acf{IPC}.\\

La comunicazione può essere sincrona o asincrona: nel primo caso, il client blocca il suo funzionamento o parte di esso attendendosi una risposta in tempi brevi; nel secondo caso, il blocco non avviene e il client continua il suo normale lavoro. 

\subsubsection{Comunicazione sincrona e \ac{REST}}
\label{REST}

Similmente ad una conversazione nella vita reale, utilizzando la comunicazione sincrona, un servizio manda una richiesta ad un altro aspettandosi una risposta in tempi brevi. \\

Un modo per realizzare questa comunicazione è utilizzare delle \acf{API} secondo l'architettura \acf{REST}. Quest'ultima sfrutta il concetto chiave di \textit{risorsa} manipolata attraverso \acf{HTTP}. Ad esempio, una richiesta \textbf{GET} si occuperà di ritornare lo stato della risorsa, come documento \acf{XML} o oggetto \acf{JSON}, sfruttando eventuali codici \ac{HTTP} per conferme ed errori.

\clearpage

\section{La cultura DevOps}


\quotingaf{"DevOps is an approach to culture, automation, and platform deisgn for delivering increased business value and responsiveness throught rapid, iterative and high-quality IT service delivery." \\ \hspace*{\fill}\citeauthor{redHatDevOps} (Red Hat) \cite{redHatDevOps}}

\subsection{La storia}

Tradizionalmente era di norma avere almeno due team separati nella realizzazione di un singolo progetto:

\begin{itemize}
    \item \textbf{Team di sviluppo} che si occupava unicamente della scrittura del codice.
    \item \textbf{Team operativo}, composto da sysadmins, network admins e database admins, dedicato alla messa in produzione del prodotto.
\end{itemize}

Mentre il team operativo era interessato a mantenere l'infrastruttura stabile evitando problemi di varia natura, il team di sviluppo pensava all'introduzione di nuove funzionalità, alla creazione di nuove versioni e infine di bug fix. Tuttavia, data la scarsa comunicazione tra i due team, molti dei problemi tendevano a nascere unicamente durante o dopo la messa in produzione del prodotto, rallentandone l'intero ciclo di sviluppo. 

\subsection{L'evoluzione del modello DevOps}

Nato dall'unione dei termini "\textit{Development}" (sviluppo) e "\textit{Operations}" (operazioni), \textbf{DevOps} rappresenta un nuovo approccio nella cultura aziendale atto a risolvere i problemi di scarsa comunicazione e collaborazione presenti nei modelli tradizionali \cite{amazonDevOps}. \\ 

Secondo questo modello, tutti i team interessati nella realizzazione di un prodotto: amministrazione, sviluppo, operativo e controllo qualità, lavorano in \begin{wrapfigure}{l}{0.5\textwidth}
	\vspace{-8pt}
	\includegraphics[width=0.49\textwidth]{devops-cycle}
	\vspace{-10pt}
\end{wrapfigure}unisono durante tutte le fasi, dallo sviluppo alla distribuzione e produzione. 
Il modello prevede la creazione di piccoli team interdisciplinari che lavorano con l'unico obiettivo di velocizzare la messa in produzione della singola funzionalità e di ricevere il corrispettivo feedback dell'utente. \\

La cultura DevOps non è unicamente un approccio legato al lavoro sul singolo progetto. Basandosi sull'approccio open source, esso prevede il "\textit{growth mindset}" \cite{microsoftDevOps}, dove l'unione di diverse competenze e la condivisione delle informazioni sono messe al primo posto. Un processo decisionale trasparente, l'invito alla sperimentazione e la meritocrazia sono i punti chiave di questa filosofia. 
\clearpage 

\section{I vantaggi dell'approccio DevOps}

L'approccio DevOps, include in sé i principi dello sviluppo Agile\footnote{Agile è un approccio dello sviluppo software atto a fornire velocemente miglioramenti per il cliente. Il tutto viene realizzato grazie alla pianificazione del lavoro ed un continuo aggiornamento ed adattamento dell'intero team.} dando un valore aggiunto ai clienti, ai fornitori, ai partner e all'azienda stessa. Tra i vantaggi di questa filosofia troviamo:

\subsubsection{Minor tempo di sviluppo}

Promuovendo la collaborazione e la comunicazione tra team, la cultura DevOps, permette ad una singola funzione di passare velocemente dallo sviluppo alla produzione. 

\subsubsection{Maggior velocità di release}

Il minor tempo di sviluppo, porta ad una conseguente velocità di rilascio e di correzioni di bug. Ciò che prima impiegava dai tre ai sei mesi, ora è ridotto a giorni se non ore.  

\subsubsection{Riduzione dei problemi}

Uno dei miglioramenti nello sviluppo è dato dalla scomposizione di software monolitici in microservizi. Ciò rende più facile la rilevazione di problemi e la loro correzione, nonché il temporaneo rollback. 

\subsubsection{Sicurezza}

Non riguardando unicamente i team di sviluppo e produzione, DevOps riesce ad ottenere il massimo in ogni campo, tra cui quello della sicurezza e sul controllo di conformità del codice. 

\clearpage

\section{Best Practices}
\label{BPDevops}

Uno dei dodici principi del \textit{Manifesto Agile} a cui fa riferimento anche la metodologia DevOps recita: 

\quotingaf{"Our highest priority is to satisfy the customer through early and continuous delivery of valuable software." \cite{manifesto2010manifesto}}

Non è un caso, quindi, che una delle prassi fondamentali del DevOps consista nell'aumentare la frequenza degli aggiornamenti riducendone la grandezza e permettendo una maggior efficienza nella successiva ricerca di problemi. 

\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{continuous_integration}
    \caption{\ac{CI} vs \ac{CD} vs Implementazione Continua \cite{amazonDevOps}.}
    \label{CICDCD}
\end{figure}

Per far ciò, un'azienda ha bisogno di processi ben definiti e strumenti automatizzati che seguano l'intero ciclo di sviluppo. L'\textbf{integrazione} e la \textbf{distribuzione continua}, permettono ad un'azienda di sviluppare velocemente in modo affidabile e sicuro. Successivamente, pratiche automatiche come l'\acf{IaC} unite ad un monitoraggio ed alla registrazione di log, permettono una gestione e un controllo delle prestazioni dell'intera infrastruttura. 


\subsection{Integrazione continua}

L'\textbf{integrazione continua}, dall'inglese \textit{\acf{CI}}, è un processo per automatizzare la creazione di build e di test ogniqualvolta uno sviluppatore esegue un commit e successivo push su un repository condiviso sfruttante un sistema di controllo della versione, ad esempio Git\footnote{Git è un software di controllo versione distribuito che permette di tenere traccia delle modifiche apportate al codice sorgente di un software. Prevede l'uso di branch ovvero diramazioni dello sviluppo dal branch base, detto master, che riguardano singole funzionalità che si uniranno successivamente in master.}. \\ 

Il commit del codice, innesca un sistema automatico di build che prende l'ultimo codice dal repository condiviso, lo compila, lo testa e lo valida secondo criteri precedentemente stabiliti, riducendo bug che verranno catturati già nel ciclo di sviluppo. \\

Sfruttando l'uso di branch separati, la \ac{CI} permette di mantenere il master pulito che andrà ad includere unicamente codice che supera tutti i criteri, riducendo notevolmente eventuali \textit{merge conflicts} e velocizzando l'aggiunta di funzionalità. 

\subsection{Distribuzione Continua}

In estensione alla \ac{CI}, abbiamo la distribuzione continua o  \textit{\acf{CD}}, che rappresenta il processo di buildare, testare, configurare e deployare codice in un ambiente di produzione. 

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{continuousdelivery}
	\caption{Distribuzione continua \cite{microsoftDevOps}.}
\end{figure}

Senza la \ac{CD}, i cicli di rilascio rappresentavano il collo di bottiglia per lo sviluppo, in quanto processi manuali e non standardizzati portavano ad inconsistenza nell'ambiente di test e quindi alla produzione di ritardi ed errori. \\

L'intero processo viene automatizzato sfruttando una \textbf{pipeline} automatica che permette di distribuire le modifiche al codice in un ambiente di testing o di produzione. L'affidabilità del processo è garantita dai test, che non rappresentando unicamente gli unit test, verificano gli aggiornamenti su vari livelli prima di renderli disponibili ai clienti. Questi test possono riguardare l'interfaccia, il caricamento, l'integrazione, l'affidabilità della comunicazione tra microservizi e quant'altro. 

\subsubsection{Distribuzione continua contro implementazione continua}

La \ac{CD} prevede unicamente il deploy in un ambiente di testing o temporaneo, lasciando poi la possibilità allo sviluppatore di utilizzare il tutto unicamente come test o di distribuirlo in produzione. Con l'\textbf{implementazione continua}, anche la produzione viene automatizzata, senza previa approvazione dello sviluppatore e quindi senza alcun lavoro manuale, com'è possibile vedere dalla figura \ref{CICDCD}. 

\subsection{\acf{IaC}}

L'infrastruttura come codice, o \acf{IaC}, è una prassi chiave del DevOps usata in congiunzione con la \ac{CD}. Essa permette la creazione \begin{wrapfigure}{l}{0.5\textwidth}
	\vspace{-8pt}
	\includegraphics[width=0.49\textwidth]{infrastructureascode}
	\vspace{-10pt}
\end{wrapfigure}e la gestione dell'intera infrastruttura attraverso metodologie di sviluppo simili a quelle usate per lo sviluppo del codice applicativo. Con la stessa sicurezza che un codice applicativo generi lo stesso binario, un modello \ac{IaC} permette di generare lo stesso ambiente ogni volta che esso viene eseguito. 

La possibilità di creare ambienti partendo dallo stesso codice sorgente, permette agli amministratori di interagire con l'infrastruttura su larga scala programmandone eventuali cambiamenti e distribuendoli con rapidità attraverso aggiornamenti. Un altro vantaggio, è la possibilità di testare applicazioni in un ambiente come quello di produzione sfruttando le medesime configurazioni.
