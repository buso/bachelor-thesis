\chapter{5G: la nuova generazione di reti mobili}
\selectlanguage{italian}


\quotingaf{"5G will have an impact similar to the introduction of electricity or the car, affecting entire economies and benefiting entire societies." \\ \hspace*{\fill}Steve Mollenkopf (Qualcomm)}

L'avanzamento tecnologico degli ultimi quarant'anni ha portato l'intera popolazione mondiale a cambiare il proprio modo di vivere e lavorare. Uno degli esempi più lampanti, è dato dalle reti mobili che, dagli anni 80 ad oggi, hanno avuto una continua evoluzione e hanno permesso a sempre più persone e dispositivi di essere connessi tra loro. \\

Non è un caso, che la nuova \acf{5G} di rete si afficcia su uno scenario dove la comunicazione mobile è una parte indispesabile della vita di milioni di persone al mondo; dove l'uso varia dalla semplice comunicazione interpersonale, all'uso di dispositivi in molteplici ambiti, tra i quali: \acf{IoT}, automobilistico, medico e industriale. \\

\section{L'evoluzione: dalla prima alla quarta generazione}

Come visto precedentemente, le prime reti mobili hanno fatto la loro apparizione all'inizio degli anni 80, con la prima generazione (1G) che dava la possibilità di effettuare unicamente chiamate vocali. All'epoca, i segnali trasmessi tra le antenne erano analogici e le chiamate sfruttavano la tecnologia \acf{FDMA}. All'epoca la rete cellulare aveva una copertura limitata, una bassa qualità per le chiamate e poca sicurezza. \\

Successivamente, agli inizi degli anni 90, la seconda generazione (2G) ha portato all'utilizzo di segnali digitali, che oltre a migliorare i servizi offerti dalla precedente generazione, ha portato allo scambio di messaggi testuali (SMS) tra cellulari, rivoluzionando il modo di comunicare. Le tecnologie utilizzate inizialmente furono \acf{GSM} e \acf{CDMA} che successivamente si sono evolute in \acf{GPRS} e \acf{EDGE}, portando lo scambio dati rispettivamente a $\sim$100kbps e $\sim$300kpbs. \\ 

\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{CellularEvolution}
    \caption{L'evoluzione delle reti cellulari}
\end{figure}

La vera evoluzione dei dati si è avuta con lo standard previsto dalla terza generazione (3G), usante le tecnologie \acf{HSPA} e \acf{EVDO}, dove la velocità si misurava in Mbps e non più in kbps. L'avanzamento tecnologico portato dalla seconda e dalla terza generazione, ha portato ad un miglioramento della qualità dei cellulari che, oltre ad avere un form-factor più ridotto, montavano migliori display, chipset più potenti e fotocamere digitali per offrire un'esperienza utente orientata al multimedia. \\

La quarta generazione della tecnologia cellulare, avviata a fine del primo decennio del ventunesimo secolo, ha portato all'introduzione degli smartphone e al maggior uso dello streaming di contenuti digitali attraverso la rete. Il tutto, offrendo velocità che vanno dalle decine alle centinaia di Mbps, sino ad arrivare a 1Gbps. Le tecnologie proposte da queste generazione sono \acf{LTE} e \acf{LTE-A} che, oltre ai vantaggi precedentemente proposti, hanno portato al completamentamento del passaggio da commutazione di circuito alla commutazione di pacchetti IP, basandosi sullo stesso funzionamento della normale rete fissa. \\

\clearpage 


\section{\acf{3GPP}: la standardizzazione}

Una delle prerogative riguardanti il \ac{5G} è la standardizzazione dello stesso, così da assicurare intercompatibilità tra dispositivi di diversi produttori. A questo proposito, molti enti, tra cui il precedentemente citato \ac{ETSI}, si sono uniti nella partnership \acf{3GPP} con l'obiettivo di standardizzare \acf{NR} e altre tecnologie. Gli standard sviluppati da \ac{3GPP} sono poi utilizzati come basi per lo sviluppo di standard locali seguiti dalle rispettive organizzazioni. \\

Il lavoro svolto da \ac{3GPP} è fatto su più fasi:

\begin{itemize}
    \item La \textbf{fase 0} si occupa di studiare come una specifica funzione possa essere sviluppata, così da ottenere una versione iniziale delle possibili specifiche.
    \item La \textbf{fase 1} descrive l'eventuale servizio dal punto di vista dell'utente. 
    \item La \textbf{fase 2} crea un'analisi logica e definisce l'architettura e le funzioni.
    \item La \textbf{fase 3} definisce la concreta implementazione e i relativi protocolli.
\end{itemize}

\begin{wrapfigure}{l}{0.5\textwidth}
	\vspace{-8pt}
	\includegraphics[width=0.49\textwidth]{Release3GPP}
    \vspace{-10pt}
    \caption{Lista delle release del \ac{3GPP}}
\end{wrapfigure}

Le funzionalità delle standardizzazioni \ac{3GPP} sono descritte in release parallele. Una release si considera da iniziare quando la fase 0 è finita e si ha un'idea generale del lavoro da svolgere mentre si considererà \textit{congelata} quando si ha un insieme di funzionalità stabili. \\
Per quanto riguarda la \ac{NR}, le release di interesse sono le 14, 15 e il 16. Quest'ultima iniziata negli ultimi mesi, in parallelo con il congelamento della 15 avvenuto lo scorso 14 Settembre 2018. 

\clearpage 

\section{Le necessità}

Nelle fasi di standardizzazione della nuova generazione e della relativa infrastruttura, sono stati considerati differenti casi d'uso che dovrebbero rappresentare al meglio l'industria dei prossimi anni \cite{5GSDBook}. L'obiettivo di questa tesi non sarà affrontarli tutti ma affrontare quelli che sono gli esempi più importanti, per poi introdurre tre macro categorie basate sui requisiti necessari per il funzionamento.

\subsubsection{Industria 4.0}

Una delle maggiori trasformazioni dell'industria attuale, conosciuta come \textbf{Industria 4.0} vedrà lo sviluppo di processi produttivi smart, facenti uso di connettività di massa, cloud computing, big analytics e automazione intelligente. I processi industriali, saranno localizzati in una singola area o distribuiti su tutto il territorio e saranno automatizzati per offrire una produzione di qualità, consistente e con costi efficienti. La linea di produzione consisterà, infatti, di differenti tipi di dispositivi, come sensori o robots, che dovranno comunicare tra loro sia in luoghi indoor che outdoor e dovranno permettere l'assistenza umana attraverso nuove tecnologie come la realtà aumentata o virtuale. Questo tipo di processi avrà bisogno di un buon grado di \textbf{affidabilità} e \textbf{bassa latenza}. 

\subsubsection{Veicoli autonomi}

La trasformazione in campo automotive è guidata attualmente dalle auto elettriche e dalla guida autonoma. Uno degli esempi più popolari è dato da \blu{Tesla} e il suo \textbf{Autopilot}. Le auto di nuove generazione potranno quindi beneficiare dei vantaggi del \ac{5G}, sfruttando la possibilità di connettere centinaia di sensori tra di loro. L'unione delle auto intelligenti, unito ai \acf{ITSs}, porterà benefici e miglioramenti nella gestione del traffico, nella qualità della vita e nella sicurezza degli utenti. \\

\subsubsection{Medicina}

Ci si aspetta che il \ac{5G} porterà nuove applicazioni anche in campo medico, dove si vedrà lo sviluppo di \textbf{dispositivi indossabili} atti a monitorare la nostra salute. Un'applicazione corrente, sono i dispositivi per il monitoraggio della frequenza cardiaca o del livello di glicemia.\\

Il 5G, grazie alla sua \textbf{bassa latenza} e \textbf{alta affidabilità}, offrirà anche la possibilità di effettuare operazioni in remoto, sfruttando robot capaci di offrire un feedback in real time al chirurgo.

\subsubsection{Media e intrattenimento}

Grazie allo sviluppo delle precedenti generazioni, negli ultimi anni il mondo ha assistito ad una crescita esponenziale di dispositivi per l'intrattenimento connessi come smartphone, tablet e TV. Ciò prevede una \textbf{maggior velocità di trasferimento di dati} e una \textbf{\acf{QoS} molto alta}.\\

Altri parametri importanti rappresentano la \textbf{scalabilità} e l'\textbf{affidabilità}, per far fronte ad alte concentrazioni di utenti come eventi ricreativi (concerti o festival) o emergenze. 

\subsection{Le tre macro categorie}

I casi d'uso del \ac{5G}, come precedentemente detto, possono essere classificati in tre differenti macro categorie: \textbf{\acf{mMTC}}, \textbf{\acf{cMTC}} o \textbf{\acf{URLLC}} e \textbf{\acf{eMBB}} \cite{Ericsson5GSystem}. 

\subsubsection{\acf{mMTC}}

Anche conosciuta come \textit{\ac{IoT} massivo}, è una categoria pensata per tutti i dispositivi che hanno bisogno una copertura molto vasta e connettività richiedente poca potenza computazionale ed energia. Molti di questi dispositivi, sono alimentati da batterie o sfruttano energie rinnovabili e sono attivi raramente; ciò permette una tolleranza ai ritardi molto alta. Un esempio di questi dispositivi è dato dal monitoraggio di infrastruttura, dall'agricoltura smart e dal tracciamento e gestione di flotte.

\subsubsection{\acf{cMTC} o \acf{URLLC}}

\acf{cMTC} o anche conosciuta come \textit{\ac{IoT} critico} è la categoria che si riferisce a tutti i dispositivi che richiedono una bassa latenza (nell'ordine dei millisecondi) e un'alta affidabilità. Tra questi troviamo, i precedentemente citati sensori per l'industria e per l'automotive. Questo tipo di requisiti è conosciuto anche come \acf{URLLC}. Inoltre, maggiore attenzione va data alla sicurezza, sia per i dispositivi \ac{cMTC} che quelli \ac{mMTC}.

\subsubsection{\acf{eMBB}}

La mobile broadband, già fornita dal 4G, sarà resa estrema offrendo un'alta velocità di trasmissione dati, una bassa latenza e un'alta copertura prevedendo, tuttavia, un calo delle performance con l'aumento dell'utenza. Questo categoria interessa l'utente comune che non ha bisogno sempre di elevate prestazioni della rete.\\

Per far fronte ai casi d'uso precedentemente descritti, il \ac{5G} porterà con sè grossi cambiamenti nello sviluppo e deployment dell'infrastruttura di rete. Le maggiori novità, possono essere trovate nell'accesso wireless e nell'architettura dove saranno sfruttate le \ac{NFV} e le \acf{SDN}. 
\clearpage 

\section{L'accesso wireless}

Uno dei componenti chiave del 5G sarà l'accesso wireless, infatti, al giorno d'oggi,
un numero sempre maggiore di dispositivi tendono a collegarsi contemporaneamente ad una rete wireless che non è cambiata nel corso del tempo. Le novità in questo campo sono date da 5 tecnologie: \textbf{Millimeter Waves, Small Cells, \acf{MIMO} massivo, Beamforming} e \textbf{Full Duplex}.

\subsection{Millimeter Waves}

Uno dei metodi per far fronte alla saturazione delle attuali frequenze, \wrapr{mmWaves} è l'estensione delle stesse introducendo nuove bande (oltre i 6GHz) che vadano anche a sfruttare quelle che sono le onde millimetriche. Quest'ultime sono generate da frequenze tra i 30 e i 300GHz, che comparate con le frequenze attualmente utilizzate (al di sotto dei 6GHz) hanno una lunghezza d'onda che varia da 1 a 10mm, contro lunghezze d'onda di decine di centimetri.\\

Considerando i differenti casi d'uso, l'utilizzo delle bande si dividerà secondo tali criteri:

\begin{itemize}
    \item le bande a basse frequenze saranno utilizzate per offrire un'alta copertura in aree molto grosse;
    \item le bande ad alta frequenze saranno utilizzate per offrire un'alta larghezza di banda, una maggior portata e alta velocità.
    \item le bande a frequenze molto alte (sopra i 24GHz), saranno utilizzate per larghezze di banda estreme.
\end{itemize}

\subsection{Small Cells}

Il \ac{5G}, a differenza delle precedenti generazioni, farà uso di celle più piccole che richiederanno poca potenza ed avranno una copertura  \wrapl{smallCells}capillare nelle città (una ogni 250 metri). Il motivo di questa scelta, è dato dall'uso delle onde millimetriche e dall'esigenza di dividere la rete tra più tipi di utenza. Questo tipo di celle, unito alla nuova interfaccia chiamata \acf{NR}, permetterà ad un singolo dispositivo di essere connesso simultaneamente a frequenze alte per lo scambio di dati temporaneo e a frequenze più basse per mantenere la copertura. 

\subsection{\ac{MIMO} massivo}

Al giorno d'oggi, il 4G, sfrutta la tecnologia \acf{MIMO} che rappresenta l'uso di due o più trasmettitori e ricevitori in modo da elaborare più dati contemporaneamente. Una stazione base del 4G  \wrapr{mMimo}  prevede una dozzina di porte per antenne, di cui otto sfruttate per la trasmissione e quattro sfruttate per la ricezione. La novità del \ac{5G} sarà l'utilizzo di centinaia di porte, così da ospitare molte più antenne in un singolo array. Ciò porterà alla possibilità di inviare e ricevere segnali da un numero di utenti più elevato e ad un uso migliore delle frequenze, nonché del segnale inviato grazie al \textbf{Beamforming}. 

\subsection{Beamforming}

Uno dei problemi del \ac{MIMO} massivo è l'interferenza nel mentre si trasmettono più informazioni da diverse antenne contemporaneamente. Un modo per ovviare al problema, è l'utilizzo di antenne intelligenti che sfruttano algoritmi avanzati per il calcolo del percorso seguito dal segnale, così da non irradiare \wrapl{Beamforming}  tutto lo spazio ma unicamente verso l'utente interessato. Non è detto, tuttavia, che l'intera trasmissione avvenga lungo  un unico percorso, ogni singolo pacchetto potrà seguire un percorso diverso ed anche rimbalzare su eventuali edifici prima di raggiungere l'utente. I vantaggi del Beamforming non riguardano unicamente il \ac{MIMO} massivo ma anche le onde millimetriche che, essendo facilmente ostacolate dagli oggetti e indebolite dalla distanza, potranno essere concentrate unicamente verso il ricevitore. 

\subsection{Full Duplex}

\wrapr{fullDuplex} Le stazioni base attuali sono limitate all'uso di più frequenze nel momento in cui l'utente vuole trasmettere e ricevere contemporaneamente. Il \ac{5G} farà uso del \textbf{Full Duplex} per raddoppiare la portata delle reti wireless semplicemente riuscendo a ricevere e trasmettere contemporaneamente sulla stessa frequenza grazie all'uso di transistori che simulano uno switch ad alta velocità. 

\clearpage


\section{L'evoluzione della \acf{CN}}

Uno degli obiettivi delle Release 15 e 16, è la standardizzazione per la \acf{5GC} documentata nelle Technical Specification 23.501 \cite{3GPPTS23501} e 23.502 \cite{3GPPTS23502}. Lo scopo è dare la possibilità agli operatori, entro il 2020, di rilasciare una rete 5G in modalità autonoma senza dipendenza dalla precedente rete LTE. 

Nel Tecnical Specification 23.501 \cite{3GPPTS23501} vengono definite l'architettura del core, gli elementi funzionali e le interfacce ad alto livello tra di loro.

\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{5garch}
    \caption{Architettura dei sistemi 5G \cite{3GPPTS23502}}
    \label{5GArch}
\end{figure}

La figura \ref{5GArch} mostra l'architettura in una situazione non-roaming, facente uso delle funzioni del \acf{CP} che vanno a connettersi tra loro attraverso interfacce basate sui servizi. L'\acf{AMF} e il \acf{SMF} si connettono al piano dei dati utente attraverso i nodi $N1$, $N2$ e $N4$ al fine di gestire la sottoscrizione, la sessione e la mobilità. Le interfacce ai nodi 2 e 3 determinano come il dispositivo si presenta al core e sono dipendenti dal network di accesso. \\ 

I maggiori componenti del core sono i seguenti:

\begin{itemize}
    \item \textbf{\acf{AMF}}: gestisce il controllo degli accessi e la mobilità, include in sè anche la \acf{NSSF}.
    \item \textbf{\acf{SMF}}: inizializza e gestisce sessioni in base alle policy della rete.
    \item \textbf{\acf{UPF}}: le UPFs possono essere messe in produzione in varie configurazioni e posizioni in base al tipo di servizio offerto. Sono gli equivalenti dei gateway nel 4G.
    \item \textbf{\acf{PCF}}: fornisce le policy tenendo conto del network slicing, del roaming e della mobilità. \El equivalente al \acf{PCRF} nel 4G.
    \item \textbf{\acf{UDM}}: raccoglie i dati e i profili dei clienti. 
    \item \textbf{\acf{NRF}}: fornisce la possibilità ad una o più \ac{NF} di conoscere altre \ac{NF}s e comunicare attraverso \ac{API}. 
    \item \textbf{\acf{NEF}}: rappresenta un gateway per le \ac{API} usate da utenti esterni, partner o enterprises per monitorare e gestire particolari policy.
    \item \textbf{\acf{AUSF}}: si occupa dell'autenticazione, come descritto dal suo nome. 
\end{itemize}

\subsection{I fondamenti del \acrlong{5GC}}

L'architettura del \acrfull{5GC} è progettata per essere \textit{cloud-native} ovvero per far uso della \ac{NFV} e delle \acf{SDN} per la realizzazione delle sue funzioni. L'aspettativa è che il deploy del \ac{5GC} avvenga su infrastrutture condivise e standardizzate. Alcuni punti chiavi usati come fondamenti per l'architettura sono:

\begin{itemize}
    \item La separazione del piano di controllo dal piano utente permette una scalabilità indipendente e un'evoluzione autonoma. Permette inoltre il deploy in diverse posizioni. 
    \item Funzioni modulari, così da creare multiple combinazioni in base al caso d'uso.
    \item Minimizzazione della dipendenza tra l'\acf{AN} e il \acf{CN}, così da creare punti d'accesso multipli e usare strumentazione indipendentemente dalla sua conformità allo standard \ac{3GPP}.
    \item Un framework di autenticazione unificato, così da sfruttare il multi accesso e "\textit{seguire}" l'utente.
    \item Supporto a funzioni \textit{stateless} dove la parte computazionale è staccata dalla parte di storage. Caratteristica derivata dalle applicazioni cloud.
    \item L'accesso concorrente ai servizi locali e centralizzati, così da supportare applicazioni che richiedono bassa latenza hostando il servizio direttamente nei data center. Ciò consegue la separazione tra piano di controllo e piano utente, dove il piano di controllo rimane centralizzato, mentre il piano utente contenente l'applicazione in uso viene spostato il più vicino possibile all'utente.  
\end{itemize}

\subsection{\acrlong{SBA} per il \acrshort{5GC}}

Nella \acf{SBA}, il \acf{NRF} permette ad uno specifico servizio di connettersi ad altri servizi senza istanziare nuove interfacce. Una \acf{NF} può essere sia produttore che consumatore di un servizio: ad esempio una \ac{NF} può richiedere ad un'altra \ac{NF} produttrice informazioni riguardanti le policy della rete. La comunicazione tra i vari servizi è basata sulla comunicazione tra microservizi, vista in \ref{REST} \cite{Huawei5G}.

\subsection{Network Slicing}

Le differenti richieste riguardanti la larghezza di banda, la latenza, l'affidabilità e quant'altro, hanno portato a creare "\textit{reti logiche capaci di fornire specifiche caratteristiche e capacità di rete}"  \cite{3GPPTS23501}, anche dette \textbf{Network Slice}.\\ 

\begin{figure}[ht]
	\centering
    \includegraphics[width=\textwidth]{NetworkSlicing}
    \caption{4G vs 5G e le network slices}
\end{figure}
 
La \acf{5G PPP} definisce le Network Slice come: 

\quotingaf{The network slice is a composition of adequately configured network functions, network applications, and the underlying cloud infrastructure (physical, virtual or even emulated resources, RAN resources etc.), that are bundled together to meet the requirements of a specific use case, e.g., bandwidth, latency, processing, and resiliency, coupled with a business purpose. \\ \hspace*{\fill} (5GPP) \cite{5GPP}}

%\quotingaf{La network slice è una composizione di funzioni ed applicazioni di rete configurate adeguatamente e dell'infrastruttura cloud sottostante unite nello scopo di venire incontro a specifici requisiti quali: larghezza di banda, latenza, resilienza e obiettivi di business.} {5GPP}


Le vecchie generazioni, prevedevano sistemi monolotici che includevano tutte le funzioni di rete (ad esempio voce, SMS o web browsing) portando difficoltà nella scalabilità e nella gestione della \ac{QoS}. 
Nelle reti 5G, grazie alla \acf{SBA} e alle Network Slice, saranno create reti logiche che condivideranno le stesse componenti fisiche. \\
